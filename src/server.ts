import express from 'express';
const app = express();
const port = process.env.PORT || 5000;
import cors from 'cors';
import path from 'path';
import * as stocks from "./stocks";

app.use(cors());
app.use(express.static('public'));

app.get('/stocks/usd', (_req, res) => {
   res.send({
      Value: stocks.USD2ILS
   });
});

app.get('/stocks', async (_req, res) => {
   res.send({
      Stocks: await stocks.getStocksData()
   });
});

app.get('/users/:user', async (req, res) => {
   res.send({
      Stocks: await stocks.getUserStocks(req.params.user)
   });
});

app.get('*', (req, res) => {
   res.sendFile(path.resolve(__dirname, 'public', 'index.html'));
});
app.listen(port, () => {
   console.log(`Server is up at port ${port}`);
});