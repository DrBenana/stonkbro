import yf from "yahoo-finance";

let stocksData = {

}

let users = {
    "Ben": {
        "QQQ": 7.07,
        "SPY": 1.27,
        "AAPL": 3.77,
        "AMZN": 0.14,
        "FB": 1.67,
        "KO": 5.74
    },
    "Aviv": {
        "QQQ": 0.92,
        "SPY": 0.42
    },
}

let usersKeys = [...Object.keys(users)];

users["Total"] = {}

usersKeys.forEach(key => {
    Object.keys(users[key]).forEach(ticker => {
        if (!users["Total"][ticker]) {
            users["Total"][ticker] = 0;
        }

        users["Total"][ticker] += users[key][ticker];
    });
})

export async function getUserStocks(user: string): Promise<{ [ticker: string]: number }> {
    if (!users[user]) {
        return {}
    }

    return users[user];
}

export async function getStocksData(): Promise<{ [ticker: string]: number }> {
    return stocksData;
}


async function getStockPrice(ticker: string): Promise<number> {
    return (await yf.quote(ticker)).price.regularMarketPrice;
}

async function loadStockData() {
    for (const user of Object.keys(users)) {
        for (const ticker of Object.keys(users[user])) {
            stocksData[ticker] = await getStockPrice(ticker);
        }
    }
}

async function reloadStockData() {
    Object.keys(stocksData).forEach(async ticker => stocksData[ticker] = getStockPrice(ticker));
}
 
loadStockData().then(() => {
    console.log("All of the stocks have been loaded!");
    setInterval(reloadStockData, 1000*60*60 /* Every hour */);
});

export let USD2ILS;

getStockPrice("USDILS=X").then(val => {
    USD2ILS = val;
})
 